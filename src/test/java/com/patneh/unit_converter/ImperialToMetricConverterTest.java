package com.patneh.unit_converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

class ImperialToMetricConverterTest {
    private ImperialToMetricConverter imperialToMetricConverter;

    @BeforeEach
    void setUp(){
        imperialToMetricConverter = new ImperialToMetricConverter();
    }

    //Tests to footToMeters(double foot)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void footToMetersWithValueSource(double value){

        //when
        double expectedResult = 0.3048 * value;
        double result = imperialToMetricConverter.footToMeters(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroFoots_shouldThrowArgumentIllegalException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {imperialToMetricConverter.footToMeters(-1);});
    }

    //Tests to limeToCentimeters(double lime)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void limeToCentimetersWithValueSource(double value){

        //when
        double expectedResult = 2.54 * value;
        double result = imperialToMetricConverter.limeToCentimeters(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroLime_shouldThrowIllegalArgumentException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {imperialToMetricConverter.limeToCentimeters(-1);});
    }

    //Tests to gallonToLiters(double gallon)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void gallonToLiteresWithValueSource(double value){

        //when
        double expectedResult = 4.5468 * value;
        double result = imperialToMetricConverter.gallonToLiters(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroGallon_shouldThrowIllegalArgumentException(){

        //when
        Assertions.assertThrows(IllegalArgumentException.class, () -> {imperialToMetricConverter.gallonToLiters(-1);});
    }

    //Tests to poundToKilograms(double pound)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void poundToKilogramsWithValueSource(double value){

        //when
        double expectedResult = 0.4536 * value;
        double result = imperialToMetricConverter.poundToKilograms(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThenZeroPound_shouldThrowIllegalArgumentException(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {imperialToMetricConverter.gallonToLiters(-1);});
    }

    //Tests to fahrenheitToCelsiusDegree(double fahrenheitDegree)

    @ParameterizedTest
    @ValueSource(doubles = {0, 32, -4, 0.5, 1000})
    void FahrenheitToCelsiusDegreeWithValueSource(double value){

        //when
        double expectedResult = (value - 32) / 1.8;
        double result = imperialToMetricConverter.fahrenheitToCelsiusDegree(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }
}
