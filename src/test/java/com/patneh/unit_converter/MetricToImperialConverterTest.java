package com.patneh.unit_converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;


class MetricToImperialConverterTest {
    private MetricToImperialConverter metricToImperialConverter;

    @BeforeEach
    void setUp(){
        metricToImperialConverter = new MetricToImperialConverter();
    }

    //Tests to metersToFoots(double meter)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void meterToFootsWithValueSource(double value){

        //when
        double expectedResult = 3.2808 * value;
        double result = metricToImperialConverter.metersToFoots(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroMeters_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> {metricToImperialConverter.metersToFoots(-1);});
    }

    //Tests to centimetersToLimes(double centimeter)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void centimetersToLimesWithValueSource(double value){

        //when
        double expectedResult = 0.3937 * value;
        double result = metricToImperialConverter.centimetersToLimes(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroCentimeters_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> {metricToImperialConverter.centimetersToLimes(-1);});
    }

    //Tests to literToGallon(double liter)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void literToGallonWithValueSource(double value){

        //when
        double expectedResult = 0.2199 * value;
        double result = metricToImperialConverter.literToGallon(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroLiters_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> {metricToImperialConverter.literToGallon(-1);});
    }

    //Tests to kilogramsToPound

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, 0.5, 1000})
    void zeroKilogram_shouldReturnZero(double value){

        //when
        double expectedResult = 2.2045 * value;
        double result = metricToImperialConverter.kilogramsToPound(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    void lessThanZeroKilograms_shouldThrowIllegalArgumentException(){

        Assertions.assertThrows(IllegalArgumentException.class, () -> {metricToImperialConverter.kilogramsToPound(-1);});
    }

    //Tests to celsiusToFahrenheitDegrees(double celsiusDegree)

    @ParameterizedTest
    @ValueSource(doubles = {0, 1, -20, 0.5, 1000})
    void celsiusToFahrenheitDegreeWithValueSource(double value){

        //when
        double expectedResult = (1.8 * value) + 32;
        double result = metricToImperialConverter.celsiusToFahrenheitDegrees(value);

        //then
        Assertions.assertEquals(expectedResult, result);
    }
}
