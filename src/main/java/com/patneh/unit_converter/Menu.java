package com.patneh.unit_converter;

import java.util.Scanner;

class Menu {
    private ImperialToMetricConverter imperialToMetricConverter = new ImperialToMetricConverter();
    private MetricToImperialConverter metricToImperialConverter = new MetricToImperialConverter();
    private Scanner scanner = new Scanner(System.in);

    Menu() {
        runningProgram();
    }

    private void runningProgram(){
        System.out.println("Witamy w konwerterze jednostek!");
        System.out.println("Z ponizszej listy wybierz co chcialbys zrobic:");
        boolean isTrue = true;
        while(isTrue) {
            menu();
            String choice = scanner.next().toUpperCase();
            switch (choice) {
                case "1":
                    metricConverter();
                    break;
                case "2":
                    imperialConverter();
                    break;
                case "EXIT":
                    isTrue = false;
                    break;
                default:
                    System.out.println("Nie znam takiego polecenia, sprobuj jeszcze raz.");
                    break;
            }
        }
    }

    private void menu(){
        System.out.println("1. Przekonwertuj jednostki z metrycznych na imperialne,");
        System.out.println("2. Przekonwertuj jednostki z imperialnych na metryczne.");
        System.out.println("EXIT");
    }


    private void metricConverter(){
        boolean isTrue = true;
        double convert;
        while (isTrue) {
            metricMenu();
            String choice = scanner.next();
            switch (choice){
                case "1":
                    convert = metricToImperialConverter.metersToFoots(converter(Types.METERS.getMessage()));
                    printValue(convert, Types.FOOT.getMessage());
                    break;
                case "2":
                    convert = metricToImperialConverter.centimetersToLimes(converter(Types.CENTIMETERS.getMessage()));
                    printValue(convert, Types.LIME.getMessage());
                    break;
                case "3":
                    convert = metricToImperialConverter.literToGallon(converter(Types.LITERS.getMessage()));
                    printValue(convert, Types.GALLON.getMessage());
                    break;
                case "4":
                    convert = metricToImperialConverter.kilogramsToPound(converter(Types.KILOGRAMS.getMessage()));
                    printValue(convert, Types.POUND.getMessage());
                    break;
                case "5":
                    convert = metricToImperialConverter.celsiusToFahrenheitDegrees(converter(Types.CELSIUS_DEGREE.getMessage()));
                    printValue(convert, Types.FAHRENHEIT_DEGREE.getMessage());
                    break;
                case "6":
                    isTrue = false;
                    break;
                default:
                    System.out.println("Nie znam takiego polecenia, sprobuj jeszcze raz!");
                    break;
            }
        }

    }

    private void metricMenu(){
        System.out.println("1. Zamien metry na stopy,");
        System.out.println("2. Zamien centymetry na cale,");
        System.out.println("3. Zamien litry na galony,");
        System.out.println("4. Zamien kilogramy na funty,");
        System.out.println("5. Zamien stopnie Celciusza na Fahrenheita,");
        System.out.println("6. Wroc do poprzedniego menu.");
    }

    private void imperialConverter(){
        boolean isTrue = true;
        double convert;
        while (isTrue) {
            imperialMenu();
            String choice = scanner.next();
            switch (choice){
                case "1":
                    convert = imperialToMetricConverter.footToMeters(converter(Types.FOOT.getMessage()));
                    printValue(convert, Types.METERS.getMessage());
                    break;
                case "2":
                    convert = imperialToMetricConverter.limeToCentimeters(converter(Types.LIME.getMessage()));
                    printValue(convert, Types.CENTIMETERS.getMessage());
                    break;
                case "3":
                    convert = imperialToMetricConverter.gallonToLiters(converter(Types.GALLON.getMessage()));
                    printValue(convert, Types.LITERS.getMessage());
                    break;
                case "4":
                    convert = imperialToMetricConverter.poundToKilograms(converter(Types.POUND.getMessage()));
                    printValue(convert, Types.KILOGRAMS.getMessage());
                    break;
                case "5":
                    convert = imperialToMetricConverter.fahrenheitToCelsiusDegree(converter(Types.FAHRENHEIT_DEGREE.getMessage()));
                    printValue(convert, Types.CELSIUS_DEGREE.getMessage());
                    break;
                case "6":
                    isTrue = false;
                    break;
                default:
                    System.out.println("Nie znam takiego polecenia, sprobuj jeszcze raz!");
                    break;
            }
        }
    }

    private void imperialMenu(){
        System.out.println("1. Zamien stopy na metry,");
        System.out.println("2. Zamien cale na centymetry,");
        System.out.println("3. Zamien galony na litry,");
        System.out.println("4. Zamien funty na kilogramy,");
        System.out.println("5. Zamien stopnie Fahrenheita na Celciusza,");
        System.out.println("6. Wroc do poprzedniego menu.");
    }

    private double converter(String message){
        converterMenu(message);
        return scanner.nextDouble();
    }

    private void converterMenu(String message){
        System.out.println("Podaj ilosc"+message+"do przeliczenia: ");
    }

    private void printValue(double result, String message){
        System.out.println("========================================================");
        System.out.println("Wynik to "+result+ message);
        System.out.println("========================================================");
    }

}
