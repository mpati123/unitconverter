package com.patneh.unit_converter;

class MetricToImperialConverter {

    private String lessThanZero = "Value should be more than 0";

    MetricToImperialConverter() { }

    double metersToFoots(double meter) {
        final double multiplier = 3.2808;
        if(meter < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * meter;
    }

    double centimetersToLimes(double centimeter) {
        final double multiplier = 0.3937;
        if(centimeter < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * centimeter;
    }

    double literToGallon(double liter) {
        final double multiplier = 0.2199;
        if(liter < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * liter;
    }

    double kilogramsToPound(double kilogram) {
        final double multiplier = 2.2045;
        if(kilogram < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * kilogram;
    }

    double celsiusToFahrenheitDegrees(double celsiusDegree) {
        final double multiplier = 1.8;
        final double addend = 32;
        return (multiplier * celsiusDegree) + addend;
    }
}
