package com.patneh.unit_converter;

class ImperialToMetricConverter {

    private String lessThanZero = "Value should be more than 0";

    double footToMeters(double foot) {
        final double multiplier = 0.3048;
        if(foot < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * foot;
    }

    double limeToCentimeters(double lime) {
        final double multiplier = 2.54;
        if(lime < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * lime;
    }

    public double gallonToLiters(double gallon) {
        final double multiplier = 4.5468;
        if(gallon < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * gallon;
    }

    double poundToKilograms(double pound) {
        final double multiplier = 0.4536;
        if(pound < 0){
            throw new IllegalArgumentException(lessThanZero);
        }
        return multiplier * pound;
    }

    double fahrenheitToCelsiusDegree(double fahrenheitDegree) {
        final double subtrahend = 32;
        final double divider = 1.8;
        return (fahrenheitDegree - subtrahend) / divider;
    }


}
