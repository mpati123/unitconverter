package com.patneh.unit_converter;

public enum Types {
    METERS(" metrów "),
    CENTIMETERS(" centymetrów "),
    LITERS(" litrów "),
    KILOGRAMS(" kilogramów "),
    CELSIUS_DEGREE(" stopni Celciusza "),
    FOOT(" stóp "),
    LIME(" cali "),
    GALLON(" galonów "),
    POUND(" funtów "),
    FAHRENHEIT_DEGREE(" stopni Fahrenheita ");

    private final String message;

    Types(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
